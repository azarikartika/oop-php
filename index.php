<?php

    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");

    // CODE DARI LINK TUGAS
    // $sheep = new Animal("shaun");

    // echo $sheep->name; // "shaun"
    // echo $sheep->legs; // 4
    // echo $sheep->cold_blooded; // "no"

    //ADA BEBERAPA YANG SAYA TAMBAHKAN SUPAYA OUPUT SESUAI DENGAN YANG DIPERINTAHKAN
    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name . "<br>"; // "shaun"
    echo "legs : " . $sheep->legs . "<br>"; // 4
    echo "cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

    // CODE DARI LINK TUGAS
    // $sungokong = new Ape("kera sakti");
    // //$sungokong->yell() // "Auooo" *terlewat ;
    // $sungokong->yell(); // "Auooo"

    // $kodok = new Frog("buduk");
    // $kodok->jump() ; // "hop hop"

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name . "<br>"; // "shaun"
    echo "legs : " . $kodok->legs . "<br>"; // 4
    echo "cold blooded : " . $kodok->cold_blooded . "<br>"; // "no"
    echo "Yell : " . $kodok->jump() ."<br><br>"; // "Auooo"
    
    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->name . "<br>"; // "shaun"
    echo "legs : " . $sungokong->legs . "<br>"; // 4
    echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
    echo "Yell : " . $sungokong->yell() ."<br>"; // "Auooo"
?>